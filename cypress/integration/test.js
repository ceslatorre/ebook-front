describe('My First Test', function() {
  it('Visits the Kitchen Sink', function() {
    cy.visit('http://localhost:3000')
    cy.get(".v-text-field__slot input").type("ansible")
    cy.contains("DevOps in Python")
    cy.get(".v-card").should("have.length", 19)
  })
})